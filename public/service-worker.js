const CACHE_NAME = "appv1";

self.addEventListener('install', function(event) {
    console.log('Service Worker : Install');
    event.waitUntil(
        caches.open(CACHE_NAME).then(function(cache) {
            return cache.addAll(
                [
                    '/',
                    'index.html',
                    'service-worker.js',
                    'manifest.json',
                    'favicon.ico',
                    'musics.json',
                    'js/',
                    'js/app.js',
                    'js/chunk-vendors.js',
                    'css/',
                    'css/app.css',
                    'css/chunk-vendors.css',
                    'static/',
                    'static/img/logo.png',
                    'static/img/covers/Blood_of_the_Saints.jpg',
                    'static/img/covers/The_Sacrament_of_Sin.jpg',
                    'static/musics/POWERWOLF_Army_Of_The_Night.mp3',
                    'static/musics/POWERWOLF-Fire_and_Forgive.mp3',
                    'media/',
                    'media/POWERWOLF_Army_Of_The_Night.mp3',
                    'media/POWERWOLF-Fire_and_Forgive.mp3',
                ]
            );
        })
    );
});

self.addEventListener('fetch', function(event) {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                // Cache hit - return response
                if (response) {
                    return response;
                }

                // IMPORTANT: Cloner la requête.
                // Une requete est un flux et est à consommation unique
                // Il est donc nécessaire de copier la requete pour pouvoir l'utiliser et la servir
                var fetchRequest = event.request.clone();

                return fetch(fetchRequest).then(
                    function(response) {
                        if (!response || response.status !== 200 || response.type !== 'basic') {
                            return response;
                        }

                        // IMPORTANT: Même constat qu'au dessus, mais pour la mettre en cache
                        var responseToCache = response.clone();

                        caches.open(CACHE_NAME)
                            .then(function(cache) {
                                cache.put(event.request, responseToCache);
                            });

                        return response;
                    }
                );
            })
    );
});


// self.addEventListener('fetch', function(event) {
//     event.respondWith(
//         caches.match(event.request).then(function(response) {
//             return response || fetch(event.request);
//         })
//     );
// });

/*
self.addEventListener('fetch', function(event) {
    console.log('Service Worker : fetch');
    event.respondWith(
        caches.open('app1')
            .then(function (cache) {
                cache.match(event.request)
                    .then(function (cacheResponse) {
                        if (cacheResponse) {
                            return cacheResponse
                        } else {
                            return fetch(event.request)
                                .then(function (networkResponse) {
                                    cache.put(event.request, networkResponse.clone())
                                    return networkResponse
                                })
                        }
                    })
            })
    );
});


// self.addEventListener('fetch', function(event) {
//     event.respondWith(
//         caches.match(event.request).then(function(response) {
//             return response || fetch(event.request);
//         })
//     );
// });
*/